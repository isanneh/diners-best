<!DOCTYPE html>
<html>
<head>
<head>
<link rel="stylesheet" type="text/css" href="http://foodie.comuv.com/styles.css" />
<link rel="stylesheet" type="text/css" href="http://foodie.comuv.com/template.css" />
<link rel="stylesheet" type="text/css" href="http://foodie.comuv.com/funny2.css" />

</head>
<style type="text/css">


 ul
{
list-style-type:none;
margin:0;
padding:0;
}
li
{
display:inline;
}

#nav
{
    padding:0;
}
#nav li 
{
    display:inline;
}
#nav li a 
{   
   font-family:Arial;
   font-size:12px;
   text-decoration: none;
   float:left;
   padding:10px;
   background-color: #333333;
   color:#ffffff;
   border-bottom:1px;
   border-bottom-color:#000000;
   border-bottom-style:solid;
}
#nav li a:hover 
{
   background-color:#9B1C26;
   padding-bottom:12px;
   border-bottom:2px;
   border-bottom-color:#000000;
   border-bottom-style:solid;
   margin:-1px;
}


</style>
</head>

<body>

<?php
//Start session
	session_start();

if(isset($_SESSION['SESS_PRIV']) && (trim($_SESSION['SESS_PRIV']) == 'user')){
echo "
 
<div id=\"navigation\">
<ul  >
<li ><a href=\"default.php\">Home</a></li>
<li><a href=\"myfood.php\">My Food</a></li>
<li><a href=\"upload.php\">Upload Food</a></li>
<li><a href=\"mypoints.php\">My Points</a></li>
<li><a href=\"edit_account.php\">Edit Account</a></li>
<li><a href=\"favorite_food.php\">My Favorite Food</a></li>
<li><a href=\"favorite_restaurant.php\">My Favorite Restaurant</a></li>
<li><a href=\"user_profile.php\">User Profile</a></li>
<li><a href=\"hangout.php\">Add a hangout</a></li>
<li><a href=\"myhangouts.php\">My Hangouts</a></li>
<li><a href=\"prizes_redeemed.php\">Prizes Redeemed</a></li>
<li><a href=\"logout.php\">Logout</a></li>
</ul>
</div>


";
}

else if(isset($_SESSION['SESS_PRIV']) && (trim($_SESSION['SESS_PRIV']) == 'restaurant')){
echo "
 
<div id=\"navigation\"> 
<ul >
<li ><a href=\"default.php\">Home</a></li>
<li><a href=\"customer_list.php\">Customer List</a></li>
<li><a href=\"customer_gain.php\">Customer Gain</a></li>
<li><a href=\"restaurant_points.php\">Customer Points</a></li>
<li><a href=\"restaurant_redeem.php\">Redeem Customer Points</a></li>
<li><a href=\"restaurant_edit.php\">Edit Restaurant Account</a></li>
<li><a href=\"upload_menu.php\">Upload Menu</a></li>
<li><a href=\"delete_menu.php\">Delete Menu</a></li>
<li><a href=\"add_prize.php\">Add Prize</a></li>
<li><a href=\"edit_prizes.php\">Edit Prize</a></li>
<li><a href=\"restaurant_profile.php\">Restaurant Profile</a></li>
<li><a href=\"restaurant_points_info.php\">Add Points Info</a></li>
<li><a href=\"restaurant_prizes_redeemed.php\">Prizes Redeemed</a></li>
<li><a href=\"logout.php\">Logout</a></li>
</ul>
</div>

";
}

else
{
echo "
<div id=\"navigation\"> 
<ul>
<li><a href=\"user_registration.php\">User Registration</a></li>
<li><a href=\"restaurant_registration.php\">Restaurant Registration</a></li>
<li><a href=\"login-form.php\">Login</a></li>
</ul>
</div>
";
}
?>
</body>
</html>