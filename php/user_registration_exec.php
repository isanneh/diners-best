<?php

	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

//email format
	function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

//name format

function isValidName($name){
return eregi ("^[a-zA-Z][a-zA-Z -]*$", $name);
}


	
	//Sanitize the POST values
	$first_name = clean($_POST['first_name']);
	$last_name = clean($_POST['last_name']);
	$email_address = clean($_POST['email_address']);
	$user_name = clean($_POST['user_name']);
	$password = clean($_POST['password']);
	$confirm_password = clean($_POST['confirm_password']);
	
	//$country = clean($_POST['country']);
	
	//Input Validations
	if($first_name == '') {
		$errmsg_arr[] = 'First name missing';
		$errflag = true;
	}


	
	if($last_name == '') {
		$errmsg_arr[] = 'Last name missing';
		$errflag = true;
	}

	if($email_address == '') {
		$errmsg_arr[] = 'Email missing';
		$errflag = true;
	}
else
{
if(isValidEmail($email_address) ==TRUE)
{
}
else
{
$errmsg_arr[] = 'Email address is not valid';
		$errflag = true;
}
}
	if($user_name == '') {
		$errmsg_arr[] = 'Login ID missing';
		$errflag = true;
	}
	if($password == '') {
		$errmsg_arr[] = 'Password missing';
		$errflag = true;
	}
	if($confirm_password == '') {
		$errmsg_arr[] = 'Confirm password missing';
		$errflag = true;
	}
	if( strcmp($password, $confirm_password) != 0 ) {
		$errmsg_arr[] = 'Passwords do not match';
		$errflag = true;
	}
	
//	if($country == 'choose_country') {
	//	$errmsg_arr[] = 'Country missing';
	//	$errflag = true;
	//}

	//Check for duplicate user login ID
	if($user_name != '') {
		$qry = "SELECT * FROM users WHERE User_Name='$user_name'";
		$result = mysql_query($qry);
		if($result) {
			if(mysql_num_rows($result) > 0) {
				$errmsg_arr[] = 'Login ID already in use';
				$errflag = true;
			}
			@mysql_free_result($result);
		}
		else {
			die("Query failed");
		}
	}

//Check for duplicate restaurant login ID
	if($user_name != '') {
		$qry = "SELECT * FROM restaurants WHERE User_Name='$user_name'";
		$result = mysql_query($qry);
		if($result) {
			if(mysql_num_rows($result) > 0) {
				$errmsg_arr[] = 'Login ID already in use';
				$errflag = true;
			}
			@mysql_free_result($result);
		}
		else {
			die("Query failed");
		}
	}

	
		//Check for duplicate user Email
	if($email_address != '') {
		$qry = "SELECT * FROM users WHERE Email_Address='$email_address'";
		$result = mysql_query($qry);
		if($result) {
			if(mysql_num_rows($result) > 0) {
				$errmsg_arr[] = 'Email address already in use';
				$errflag = true;
			}
			@mysql_free_result($result);
		}
		else {
			die("Query failed");
		}
	}

//Check for duplicate restaurant email
	if($email_address != '') {
		$qry = "SELECT * FROM restaurants WHERE Email_Address='$email_address'";
		$result = mysql_query($qry);
		if($result) {
			if(mysql_num_rows($result) > 0) {
				$errmsg_arr[] = 'Email Address already in use';
				$errflag = true;
			}
			@mysql_free_result($result);
		}
		else {
			die("Query failed");
		}
	}

	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: user_registration.php");
		exit();
	}
$profile_url= $user_name;
$profile_url .=".php";
	//Create INSERT query
	$qry = "INSERT INTO users(First_Name, Last_Name, User_Name, Email_Address, Password, Profile_Url) VALUES('$first_name','$last_name', '$user_name', '$email_address','".md5($_POST['password'])."',
        '$profile_url')";
	$result = @mysql_query($qry);
	
	//Check whether the query was successful or not
	if($result) {
			$_SESSION['SESS_USER_NAME'] = $user_name;
			$_SESSION['SESS_EMAIL'] = $email_address;
$directory .=$_SESSION['SESS_USER_NAME'];
mkdir("user/" . $directory, 0777, true); 
		header("location: user_registration_success.php");
		exit();
	}else {
		die("Query failed");
	}
?>