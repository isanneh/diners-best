<?php

	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

//email format
	function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

//name format

function isValidName($name){
return eregi ("^[a-zA-Z][a-zA-Z -]*$", $name);
}


	
	//Sanitize the POST values
	$current_password = clean($_POST['current_password']);
	$new_password = clean($_POST['new_password']);
        $confirm_password = clean($_POST['confirm_password']);
	$current_email_address = clean($_POST['current_email_address']);
        $new_email_address = clean($_POST['new_email_address']);
	$restaurant_name = clean($_POST['restaurant_name']);
	$restaurant_category = clean($_POST['restaurant_category']);
	$other_category = clean($_POST['other_category']);
$phone = clean($_POST['phone']);
$website = clean($_POST['website']);
$street = clean($_POST['street']);
$city = clean($_POST['city']);
$state = clean($_POST['state']);
$zipcode = clean($_POST['zipcode']);
$description = clean($_POST['description']);
$monday_open= clean($_POST['monday_open']);
$monday_close= clean($_POST['monday_close']);
$tuesday_open= clean($_POST['tuesday_open']);
$tuesday_close= clean($_POST['tuesday_close']);
$wednesday_open= clean($_POST['wednesday_open']);
$wednesday_close= clean($_POST['wednesday_close']);
$thursday_open= clean($_POST['thursday_open']);
$thursday_close= clean($_POST['thursday_close']);
$friday_open= clean($_POST['friday_open']);
$friday_close= clean($_POST['friday_close']);
$saturday_open= clean($_POST['saturday_open']);
$saturday_close= clean($_POST['saturday_close']);
$sunday_open= clean($_POST['sunday_open']);
$sunday_close= clean($_POST['sunday_close']);

	
	

switch($_POST['submit']){
	
		case 'Change Password':

//Input Validations
	if($current_password == '') {
		$errmsg_arr[] = 'Current password missing';
		$errflag = true;
	}


	
	if($new_password == '') {
		$errmsg_arr[] = 'New Password missing';
		$errflag = true;
	}

	if($confirm_password == '') {
		$errmsg_arr[] = 'Confirm Password missing';
		$errflag = true;
	}

	if( strcmp($new_password, $confirm_password) != 0 ) {
		$errmsg_arr[] = 'New Passwords do not match';
		$errflag = true;
	}
	
//check password
$restaurant_id=$_SESSION['SESS_RESTAURANT_ID'];
//Create query
	$qry="SELECT * FROM restaurants WHERE `Restaurant_Id`='$restaurant_id' AND `Password`='".md5($_POST['current_password'])."'";
	$result=mysql_query($qry);

	//Check whether the query was successful or not
	if($result) {
		if(mysql_num_rows($result) == 1) {
			//Correct Password 
			mysql_query("UPDATE `restaurants` SET `Password` ='".md5($new_password)."' where `Restaurant_Id` = '$restaurant_id'");
			header("location: restaurant_profile.php");
			exit();
		}
      else {
		$errmsg_arr[] = 'Incorrect Password';
		$errflag = true;
	}

}
else {
		die("Query failed");
	}

	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: restaurant_edit.php");
		exit();
	}

			
			break;
		case 'Change Email Address':

//Input Validations
	if($current_password == '') {
		$errmsg_arr[] = 'Current password missing';
		$errflag = true;
	}

	
	if($current_email_address == '') {
		$errmsg_arr[] = 'Current Email Address missing';
		$errflag = true;
	}

if($new_email_address == '') {
		$errmsg_arr[] = 'New Email Address missing';
		$errflag = true;
	}
else
{
if(isValidEmail($new_email_address) ==TRUE)
{
}
else
{
$errmsg_arr[] = 'New Email address is not valid';
		$errflag = true;
}
}

	
	
//check password
$restaurant_id=$_SESSION['SESS_RESTAURANT_ID'];
//Create query
	$qry="SELECT * FROM restaurants WHERE `Restaurant_Id`='$restaurant_id' AND `Password`='".md5($_POST['current_password'])."'";
	$result=mysql_query($qry);

	//Check whether the query was successful or not
	if($result) {
		if(mysql_num_rows($result) == 1) {
			//Correct Password 
		
		}
      else {
		$errmsg_arr[] = 'Incorrect Password';
		$errflag = true;
	}

}
else {
		die("Query failed");
	}

//check email address
$email_address=$_SESSION['SESS_EMAIL_ADDRESS'];
$restaurant_id=$_SESSION['SESS_RESTAURANT_ID'];
//Create query
	$qry="SELECT * FROM restaurants WHERE `Email_Address`='$email_address' AND `Password`='".md5($_POST['current_password'])."'";
	$result=mysql_query($qry);

	//Check whether the query was successful or not
	if($result) {
		if(mysql_num_rows($result) == 1) {
			//Correct Email 
			mysql_query("UPDATE `restaurants` SET `Email_Address` ='$new_email_address' where `Restaurant_Id` = '$restaurant_id'");
			header("location: restaurant_profile.php");
			exit();
		}
      else {
		$errmsg_arr[] = 'Incorrect Email Address';
		$errflag = true;
	}

}
else {
		die("Query failed");
	}

	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: restaurant_edit.php");
		exit();
	}
			
			break;
                case 'Save Changes':
//update restaurant details
$user_id=$_SESSION['SESS_USER_ID'];

//Input Validations
	if($restaurant_name == '') {
		$errmsg_arr[] = 'Restaurant name missing';
		$errflag = true;
	}

if($restaurant_category == 'choose_restaurant' && $other_category == '' ) {
		$errmsg_arr[] = 'Category missing';
		$errflag = true;
	}

else
{ 

if($restaurant_category == 'choose_restaurant') {
		$chosen_category=$other_category;
	}

else
{

if($other_category == '' ) {
		$chosen_category=$restaurant_category;
	}
}
}






if($phone == '') {
		$errmsg_arr[] = 'Phone missing';
		$errflag = true;
	}

if($street == '') {
		$errmsg_arr[] = 'Street missing';
		$errflag = true;
	}

if($city == '') {
		$errmsg_arr[] = 'City missing';
		$errflag = true;
	}

if($zipcode == '') {
		$errmsg_arr[] = 'Zipcode missing';
		$errflag = true;
	}
	
if (isset($_POST['closed_monday']))
{
$monday_open='0';
$monday_close='0';
}
else
{	
if($monday_open == 'time') {
		$errmsg_arr[] = 'Monday open time missing. Check "Resturant is closed" if your restuarant
is closed on Mondays';
		$errflag = true;
	}
	
	if($monday_close == 'time') {
		$errmsg_arr[] = 'Monday close time missing. Check "Resturant is closed" if your restuarant
is closed on Mondays';
		$errflag = true;
	}
}

	

if (isset($_POST['closed_tuesday']))
{
$tuesday_open='0';
$tuesday_close='0';
}
else
{	
if($tuesday_open == 'time') {
		$errmsg_arr[] = 'Tuesday open time missing. Check "Resturant is closed" if your restuarant
is closed on Tuesdays';
		$errflag = true;
	}
	
	if($tuesday_close == 'time') {
		$errmsg_arr[] = 'Tuesday close time missing. Check "Resturant is closed" if your restuarant
is closed on Tuesdays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_wednesday']))
{
$wednesday_open='0';
$wednesday_close='0';
}
else
{	
if($wednesday_open == 'time') {
		$errmsg_arr[] = 'Wednesday open time missing. Check "Resturant is closed" if your restuarant
is closed on Wednesdays';
		$errflag = true;
	}
	
	if($wednesday_close == 'time') {
		$errmsg_arr[] = 'Wednesday close time missing. Check "Resturant is closed" if your restuarant
is closed on Wednesdays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_thursday']))
{
$thursday_open='0';
$thursday_close='0';
}
else
{	
if($thursday_open == 'time') {
		$errmsg_arr[] = 'Thursday open time missing. Check "Resturant is closed" if your restuarant
is closed on Thursdays';
		$errflag = true;
	}
	
	if($thursday_close == 'time') {
		$errmsg_arr[] = 'Thursday close time missing. Check "Resturant is closed" if your restuarant
is closed on Thursdays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_friday']))
{
$friday_open='0';
$friday_close='0';
}
else
{	
if($friday_open == 'time') {
		$errmsg_arr[] = 'Friday open time missing. Check "Resturant is closed" if your restuarant
is closed on Fridays';
		$errflag = true;
	}
	
	if($friday_close == 'time') {
		$errmsg_arr[] = 'Friday close time missing. Check "Resturant is closed" if your restuarant
is closed on Fridays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_saturday']))
{
$saturday_open='0';
$saturday_close='0';
}
else
{	
if($saturday_open == 'time') {
		$errmsg_arr[] = 'Saturday open time missing. Check "Resturant is closed" if your restuarant
is closed on Saturdays';
		$errflag = true;
	}
	
	if($saturday_close == 'time') {
		$errmsg_arr[] = 'Saturday close time missing. Check "Resturant is closed" if your restuarant
is closed on Saturdays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_sunday']))
{
$sunday_open='0';
$sunday_close='0';
}
else
{	
if($sunday_open == 'time') {
		$errmsg_arr[] = 'Sunday open time missing. Check "Resturant is closed" if your restuarant
is closed on Sundays';
		$errflag = true;
	}
	
	if($sunday_close == 'time') {
		$errmsg_arr[] = 'Sunday close time missing. Check "Resturant is closed" if your restuarant
is closed on Sundays';
		$errflag = true;
	}
}

	if($email_address == '') {
		$errmsg_arr[] = 'Email missing';
		$errflag = true;
	}
else
{
if(isValidEmail($email_address) ==TRUE)
{
}
else
{
$errmsg_arr[] = 'Email address is not valid';
		$errflag = true;
}
}
	if($user_name == '') {
		$errmsg_arr[] = 'Login ID missing';
		$errflag = true;
	}
	if($password == '') {
		$errmsg_arr[] = 'Password missing';
		$errflag = true;
	}
	if($confirm_password == '') {
		$errmsg_arr[] = 'Confirm password missing';
		$errflag = true;
	}
	if( strcmp($password, $confirm_password) != 0 ) {
		$errmsg_arr[] = 'Passwords do not match';
		$errflag = true;
	}

$restaurant_id=$_SESSION['SESS_RESTAURANT_ID'];

  //Update query restaurant info
	 //    mysql_query("UPDATE `restaurant_info` SET `Street`='$street', `City`='$city', `State`='$state', `Zipcode`='$zipcode', `Country`='$country',
//`Description`='$description', `Website`='$website', `Phone`='$phone' WHERE `Restaurant_Id` = '$restaurant_id'");
          

    //Update query restaurant info
	     mysql_query("UPDATE `restaurant_info` SET `Street`='$street' WHERE `Restaurant_Id` = '$restaurant_id'");
             mysql_query("UPDATE `restaurant_info` SET `City`='$city' WHERE `Restaurant_Id` = '$restaurant_id'");
             mysql_query("UPDATE `restaurant_info` SET `State`='$state' WHERE `Restaurant_Id` = '$restaurant_id'");
             mysql_query("UPDATE `restaurant_info` SET  `Zipcode`='$zipcode' WHERE `Restaurant_Id` = '$restaurant_id'");
             mysql_query("UPDATE `restaurant_info` SET `Country`='$country' WHERE `Restaurant_Id` = '$restaurant_id'");
             mysql_query("UPDATE `restaurant_info` SET `Description`='$description' WHERE `Restaurant_Id` = '$restaurant_id'");
             mysql_query("UPDATE `restaurant_info` SET `Website`='$website' WHERE `Restaurant_Id` = '$restaurant_id'");
             mysql_query("UPDATE `restaurant_info` SET `Phone`='$phone' WHERE `Restaurant_Id` = '$restaurant_id'");

//Update Restaurant hours
//mysql_query("UPDATE `restaurant_hours` SET `Monday_Open`='$monday_open', `Monday_Close`='$monday_close', `Tuesday_Open`='$tuesday_open', `Tuesday_Close`='$tuesday_close,
//`Wednesday_Open`='$wednesday_open', `Wednesday_Close`='$wednesday_close', `Thursday_Open`='$thursday_open', `Thursday_Close`='$thursday_close', `Friday_Open`='$friday_open',
//`Friday_Close`='$friday_close', `Saturday_Open`='$saturday_open', `Saturday_Close`='$saturday_close', `Sunday_Open`='$sunday_open', `Sunday_Close`='$sunday_close'
//WHERE `Restaurant_Id` = '$restaurant_id'");

//Update Restaurant hours
mysql_query("UPDATE `restaurant_hours` SET `Monday_Open`='$monday_open' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Monday_Close`='$monday_close' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Tuesday_Open`='$tuesday_open' WHERE `Restaurant_Id` = '$restaurant_id'");

mysql_query("UPDATE `restaurant_hours` SET `Tuesday_Close`='$tuesday_close' WHERE `Restaurant_Id` = '$restaurant_id'");

mysql_query("UPDATE `restaurant_hours` SET `Wednesday_Open`='$wednesday_open' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Wednesday_Close`='$wednesday_close' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET  `Thursday_Open`='$thursday_open' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET  `Thursday_Close`='$thursday_close' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Friday_Open`='$friday_open' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Friday_Close`='$friday_close' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Saturday_Open`='$saturday_open' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Saturday_Close`='$saturday_close' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Sunday_Open`='$sunday_open' WHERE `Restaurant_Id` = '$restaurant_id'");
mysql_query("UPDATE `restaurant_hours` SET `Sunday_Close`='$sunday_close' WHERE `Restaurant_Id` = '$restaurant_id'");

                             
  header("location: restaurant_profile.php");
			exit();
                              			
			break;
	}
	
	
?>