<?php

	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

//email format
	function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

//name format

function isValidName($name){
return eregi ("^[a-zA-Z][a-zA-Z -]*$", $name);
}


	
	//Sanitize the POST values
	$current_password = clean($_POST['current_password']);
	$new_password = clean($_POST['new_password']);
        $confirm_password = clean($_POST['confirm_password']);
	$current_email_address = clean($_POST['current_email_address']);
        $new_email_address = clean($_POST['new_email_address']);
	$about = clean($_POST['about']);

switch($_POST['submit']){
	
		case 'Change Password':

//Input Validations
	if($current_password == '') {
		$errmsg_arr[] = 'Current password missing';
		$errflag = true;
	}


	
	if($new_password == '') {
		$errmsg_arr[] = 'New Password missing';
		$errflag = true;
	}

	if($confirm_password == '') {
		$errmsg_arr[] = 'Confirm Password missing';
		$errflag = true;
	}

	if( strcmp($new_password, $confirm_password) != 0 ) {
		$errmsg_arr[] = 'New Passwords do not match';
		$errflag = true;
	}
	
//check password
$user_id=$_SESSION['SESS_USER_ID'];
//Create query
	$qry="SELECT * FROM users WHERE `User_Id`='$user_id' AND `Password`='".md5($_POST['current_password'])."'";
	$result=mysql_query($qry);

	//Check whether the query was successful or not
	if($result) {
		if(mysql_num_rows($result) == 1) {
			//Correct Password 
			mysql_query("UPDATE `users` SET `Password` ='".md5($new_password)."' where `User_Id` = '$user_id'");
			header("location: user_profile.php");
			exit();
		}
      else {
		$errmsg_arr[] = 'Incorrect Password';
		$errflag = true;
	}

}
else {
		die("Query failed");
	}

	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: edit_account.php");
		exit();
	}

			
			break;
		case 'Change Email Address':

//Input Validations
	if($current_password == '') {
		$errmsg_arr[] = 'Current password missing';
		$errflag = true;
	}

	
	if($current_email_address == '') {
		$errmsg_arr[] = 'Current Email Address missing';
		$errflag = true;
	}

if($new_email_address == '') {
		$errmsg_arr[] = 'New Email Address missing';
		$errflag = true;
	}
else
{
if(isValidEmail($new_email_address) ==TRUE)
{
}
else
{
$errmsg_arr[] = 'New Email address is not valid';
		$errflag = true;
}
}

	
	
//check password
$user_id=$_SESSION['SESS_USER_ID'];
//Create query
	$qry="SELECT * FROM users WHERE `User_Id`='$user_id' AND `Password`='".md5($_POST['current_password'])."'";
	$result=mysql_query($qry);

	//Check whether the query was successful or not
	if($result) {
		if(mysql_num_rows($result) == 1) {
			//Correct Password 
		
		}
      else {
		$errmsg_arr[] = 'Incorrect Password';
		$errflag = true;
	}

}
else {
		die("Query failed");
	}

//check email address
$email_address=$_SESSION['SESS_EMAIL_ADDRESS'];
$user_id=$_SESSION['SESS_USER_ID'];
//Create query
	$qry="SELECT * FROM users WHERE `Email_Address`='$email_address' AND `Password`='".md5($_POST['current_password'])."'";
	$result=mysql_query($qry);

	//Check whether the query was successful or not
	if($result) {
		if(mysql_num_rows($result) == 1) {
			//Correct Email 
			mysql_query("UPDATE `users` SET `Email_Address` ='$new_email_address' where `User_Id` = '$user_id'");
			header("location: user_profile.php");
			exit();
		}
      else {
		$errmsg_arr[] = 'Incorrect Email Address';
		$errflag = true;
	}

}
else {
		die("Query failed");
	}

	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: edit_account.php");
		exit();
	}
			
			break;
                case 'Save Changes':
//update about me
$user_id=$_SESSION['SESS_USER_ID'];

//Add about in table
			mysql_query("UPDATE `users` SET `About` ='$about' where `User_Id` = '$user_id'");
			header("location: user_profile.php");
			exit();
	

			
			break;
	}
	
	
?>