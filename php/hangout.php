<?php
include 'menu.php';
if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		echo '<ul class="err">';
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo '<li>',$msg,'</li>'; 
		}
		echo '</ul>';
		unset($_SESSION['ERRMSG_ARR']);
	}
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="http://foodie.comuv.com/styles.css" />
</head>
<body>
<div id="container">




<div id="content-container1">




<div id="content-container3">




		<div id="content-container2">








			<div id="content">
<h2> Add a hangout </h2>
<form action="hangout_exec.php" method="post">
<table>
<tr>
<td>Title: </td> <td> <input type="text" name="title" /></td>
</tr>
<tr>
<td><h3> Description: </h3></td>
<td><textarea rows="10" cols="60" name="description">
</textarea>
</td>
</tr>

<tr>
<td>Restaurant Username: </td> <td> <input type="text" name="username" /></td>
</tr>
<tr>
<td>Number of people: </td> <td> <input type="text" name="people" /></td>
</tr>
<tr>
<td>Check this box if any amount of people can join hangout </td> <td> <input type="checkbox" name="any"></td>
</tr>
</tr>
<tr>
<td>Accessibility: </td> <td>
<select name="category">
<option value="public">public</option>
<option value="private">private</option>
<option value="invisible">invisible</option>
</select></td>
</tr>
<tr>
<td>Date: </td> <td>
<select name="month">
<option value="month">month</option>
<option value="January">January</option>
<option value="February">February</option>
<option value="March">March</option>
<option value="April">April</option>
<option value="May">May</option>
<option value="June">June</option>
<option value="July">July</option>
<option value="August">August</option>
<option value="September">September</option>
<option value="October">October</option>
<option value="November">November</option>
<option value="December">December</option>
</select>
<select name="day">
<option value="day">day</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>
<select name="year">
<option value="year">year</option>
<option value="2012">2012</option>
<option value="2012">2013</option>
</select></td>
</tr>
<tr>
<td>Time: </td> <td>
<select name="time">
    <option value="time">choose time</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select></td>
</tr>
<tr>
<td><input type="submit" name="submit" id="submit" value="Submit" /></td>
</tr>
</table>
</form>
</div>				
</div>
</div>
</div>
</div>
</body>
</html>		