<?php

	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}


	
	//Sanitize the POST values
$username = clean($_POST['username']);
	$title = clean($_POST['title']);
	$description = clean($_POST['description']);
	$people = clean($_POST['people']);
        $any = clean($_POST['any']);
	$day = clean($_POST['day']);
	$month = clean($_POST['month']);
        $year = clean($_POST['year']);
	$category = clean($_POST['category']);
	$time = clean($_POST['time']);

	
	//Input Validations
if($username == '') {
		$errmsg_arr[] = 'Restaurant username missing';
		$errflag = true;
	}

	if($title == '') {
		$errmsg_arr[] = 'Title missing';
		$errflag = true;
	}

//change category to choose category... or live it like that... cos it's public right now
if($category == '') {
		$errmsg_arr[] = 'Accessibility missing';
		$errflag = true;
	}


	
	if($day == 'day') {
		$errmsg_arr[] = 'day missing';
		$errflag = true;
	}

	if($month == 'month') {
		$errmsg_arr[] = 'Month missing';
		$errflag = true;
	}

if($year == 'year') {
		$errmsg_arr[] = 'Year missing';
		$errflag = true;
	}
if($time == 'choose time') {
		$errmsg_arr[] = 'Time missing';
		$errflag = true;
	}

if (isset($_POST['any']))
{
$people='0';
}
else
{ 
if($people=='') {
$errmsg_arr[] = 'Enter number of people or mark the checkbox';
		$errflag = true;
	}
}

	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: hangout.php");
		exit();
	}

$qry= "SELECT * FROM restaurants where `User_Name`='$username'";
$result=mysql_query($qry);


if($result)
{
$row=mysql_fetch_assoc($result);
$id=$row['Restaurant_Id'];
}
else
{
die("Query failed1!");
}


	//Create INSERT query
	$qry2 = "INSERT INTO hangouts(Restaurant_Id, Creator_Id, Title, Description, People, Accessibility, Month, Day, Year, Time) 
VALUES('".$row['Restaurant_Id']."', '".$_SESSION['SESS_USER_ID']."', '$title', '$description', '$people', '$category', '$month', '$day', '$year', '$time')";
	$result2 = @mysql_query($qry2);



	
	//Check whether the query was successful or not
	if($result2) {
$_SESSION['SESS_RESTAURANT_ID']=$row['Restaurant_Id'];
		header("location: hangout_success.php");
		exit();
	}else {
		die("Query failed2");
	}
?>