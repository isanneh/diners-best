<?php
session_start();
include 'menu.php';


	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		echo '<ul class="err">';
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo '<li>',$msg,'</li>'; 
		}
		echo '</ul>';
		unset($_SESSION['ERRMSG_ARR']);
	}

?>


<html>

<body>
<div id="container">

<div id="content-container1">

<div id="content-container3">

		<div id="content-container2">


			<div id="content">
<h2> Restaurant Registration </h2>

<h3> Login Information </h3>
<form action="restaurant_registration_exec.php" method="post">
  Email Address: <input type="text" name="email_address" /><br />
  User Name <input type="text" name="user_name" /><br />
  Password: <input type="password" name="password" /><br />
  Confirm Password: <input type="password" name="confirm_password" /><br />
<br />

<h3> Restaurant Details </h3>
  Name of Restaurant: <input type="text" name="restaurant_name" /><br />
<select name="restaurant_category">
    <option value="choose_restaurant">Restaurant type</option>
    <option value="italian">Italian</option>
   <option value="chinese">Chinese</option>
   <option value="pizzeria">Pizzeria</option>
  </select>
<br />
Enter restaurant type if none of the options above describe your restaurant: <input type="text" name="other_category" /><br />
  Street: <input type="text" name="street" /><br />
  City: <input type="text" name="city" /><br />
  <select name="state">
    <option value="state">Choose a state</option>
    <option value="ny">New York</option>
  </select>
<br />
  Zipcode: <input type="text" name="zipcode" /><br />
  Phone: <input type="text" name="phone" /><br />
  Website: <input type="text" name="website" /><br />

<h3> Hours of operation </h3>
<b>Monday </b>
<br />
Open: 
<select name="monday_open">
    <option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
Close:
<select name="monday_close">
    <option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
<input type="checkbox" name="closed_monday" /> Restaurant is closed 
<br />

<b>Tuesday </b>
<br />
Open: 
<select name="tuesday_open">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
Close:
<select name="tuesday_close">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
<input type="checkbox" name="closed_tuesday" /> Restaurant is closed 
<br />

<b>Wednesday </b>
<br />
Open: 
<select name="wednesday_open">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
Close:
<select name="wednesday_close">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
<input type="checkbox" name="closed_wednesday"  /> Restaurant is closed 
<br />

<b>Thursday </b>
<br />
Open: 
<select name="thursday_open">
<option value="time">Choose time.</option>
   <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
Close:
<select name="thursday_close">
<option value="time">Choose time.</option>
    <option value="24">12a.m.</option>
    <option value="1">1a.m.</option>
    <option value="2">2a.m.</option>
    <option value="3">3a.m.</option>
    <option value="4">4a.m.</option>
</select>
<input type="checkbox" name="closed_thursday"  /> Restaurant is closed 
<br />

<b>Friday </b>
<br />
Open: 
<select name="friday_open">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
Close:
<select name="friday_close">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
<input type="checkbox" name="closed_friday"  /> Restaurant is closed 
<br />

<b>Saturday </b>
<br />
Open: 
<select name="saturday_open">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
Close:
<select name="saturday_close">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
<input type="checkbox" name="closed_saturday"  /> Restaurant is closed 
<br />

<b>Sunday </b>
<br />
Open: 
<select name="sunday_open">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
Close:
<select name="sunday_close">
<option value="time">Choose time.</option>
    <option value="12 a.m.">12 a.m.</option>
    <option value="1 a.m.">1 a.m.</option>
    <option value="2 a.m.">2 a.m.</option>
    <option value="3 a.m.">3 a.m.</option>
    <option value="4 a.m.">4 a.m.</option>
    <option value="5 a.m.">5 a.m.</option>
    <option value="6 a.m.">6 a.m.</option>
    <option value="7 a.m.">7 a.m.</option>
    <option value="8 a.m.">8 a.m.</option>
    <option value="9 a.m.">9 a.m.</option>
    <option value="10 a.m.">10 a.m.</option>
    <option value="11 a.m.">11 a.m.</option>
    <option value="12 p.m.">12 p.m.</option>
    <option value="1 p.m.">1 p.m.</option>
    <option value="2 p.m.">2 p.m.</option>
    <option value="3 p.m.">3 p.m.</option>
    <option value="4 p.m.">4 p.m.</option>
    <option value="5 p.m.">5 p.m.</option>
    <option value="6 p.m.">6 p.m.</option>
    <option value="7 p.m.">7 p.m.</option>
    <option value="8 p.m.">8 p.m.</option>
    <option value="9 p.m.">9 p.m.</option>
    <option value="10 p.m.">10 p.m.</option>
    <option value="11 p.m.">11 p.m.</option>
</select>
<input type="checkbox" name="closed_sunday"  /> Restaurant is closed 
<br />

<h3> Description of restaurant </h3>
<textarea rows="10" cols="40" name="description">
Description of restaurant 
</textarea>
<br />


  <input type="submit" name="submit" id="submit" value="Submit" />
</form>	
</div>				
</div>
</div>
</div>
</div>
</body>
</html>





						