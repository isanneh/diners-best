<?php

	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

//email format
	function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

//name format

function isValidName($name){
return eregi ("^[a-zA-Z][a-zA-Z -]*$", $name);
}


	
	//Sanitize the POST values
	$title = clean($_POST['title']);
	$description = clean($_POST['description']);
	$points = clean($_POST['points']);

	
	//Input Validations
	if($title == '') {
		$errmsg_arr[] = 'Title missing';
		$errflag = true;
	}


	
	if($description == '') {
		$errmsg_arr[] = 'Description missing';
		$errflag = true;
	}

	if($points == '') {
		$errmsg_arr[] = 'Points missing';
		$errflag = true;
	}

	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: add_prize.php");
		exit();
	}

$restaurant_id=$_SESSION['SESS_RESTAURANT_ID'];


	//Create INSERT query
	$qry = "INSERT INTO prizes(Restaurant_Id, Title, Description, Points) VALUES('$restaurant_id','$title', '$description', '$points')";
	$result = @mysql_query($qry);
	
	//Check whether the query was successful or not
	if($result) {
		header("location: restaurant_home.php");
		exit();
	}else {
		die("Query failed");
	}
?>