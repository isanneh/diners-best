<?php
	//Start session
	session_start();
	
	//Include database connection details
	//require_once('connect.php');
	include("connect.php");
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

//email format
	function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

//name format

function isValidName($name){
return eregi ("^[a-zA-Z][a-zA-Z -]*$", $name);
}


	
	//Sanitize the POST values
	$restaurant_name = clean($_POST['restaurant_name']);
	$restaurant_category = clean($_POST['restaurant_category']);
	$other_category = clean($_POST['other_category']);
$phone = clean($_POST['phone']);
$website = clean($_POST['website']);
$street = clean($_POST['street']);
$city = clean($_POST['city']);
$state = clean($_POST['state']);
$zipcode = clean($_POST['zipcode']);
$email_address = clean($_POST['email_address']);
	$user_name = clean($_POST['user_name']);
	$password = clean($_POST['password']);
	$confirm_password = clean($_POST['confirm_password']);
$description = clean($_POST['description']);
$monday_open= clean($_POST['monday_open']);
$monday_close= clean($_POST['monday_close']);
$tuesday_open= clean($_POST['tuesday_open']);
$tuesday_close= clean($_POST['tuesday_close']);
$wednesday_open= clean($_POST['wednesday_open']);
$wednesday_close= clean($_POST['wednesday_close']);
$thursday_open= clean($_POST['thursday_open']);
$thursday_close= clean($_POST['thursday_close']);
$friday_open= clean($_POST['friday_open']);
$friday_close= clean($_POST['friday_close']);
$saturday_open= clean($_POST['saturday_open']);
$saturday_close= clean($_POST['saturday_close']);
$sunday_open= clean($_POST['sunday_open']);
$sunday_close= clean($_POST['sunday_close']);

	
	//Input Validations
	if($restaurant_name == '') {
		$errmsg_arr[] = 'Restaurant name missing';
		$errflag = true;
	}

if($restaurant_category == 'choose_restaurant' && $other_category == '' ) {
		$errmsg_arr[] = 'Category missing';
		$errflag = true;
	}

else
{ 

if($restaurant_category == 'choose_restaurant') {
		$chosen_category=$other_category;
	}

else
{

if($other_category == '' ) {
		$chosen_category=$restaurant_category;
	}
}
}






if($phone == '') {
		$errmsg_arr[] = 'Phone missing';
		$errflag = true;
	}

if($street == '') {
		$errmsg_arr[] = 'Street missing';
		$errflag = true;
	}

if($city == '') {
		$errmsg_arr[] = 'City missing';
		$errflag = true;
	}

if($zipcode == '') {
		$errmsg_arr[] = 'Zipcode missing';
		$errflag = true;
	}

if($website == '') {
		$$website='N/A';
	}
	
if (isset($_POST['closed_monday']))
{
$monday_open='0';
$monday_close='0';
}
else
{	
if($monday_open == 'time') {
		$errmsg_arr[] = 'Monday open time missing. Check "Resturant is closed" if your restuarant
is closed on Mondays';
		$errflag = true;
	}
	
	if($monday_close == 'time') {
		$errmsg_arr[] = 'Monday close time missing. Check "Resturant is closed" if your restuarant
is closed on Mondays';
		$errflag = true;
	}
}

	

if (isset($_POST['closed_tuesday']))
{
$tuesday_open='0';
$tuesday_close='0';
}
else
{	
if($tuesday_open == 'time') {
		$errmsg_arr[] = 'Tuesday open time missing. Check "Resturant is closed" if your restuarant
is closed on Tuesdays';
		$errflag = true;
	}
	
	if($tuesday_close == 'time') {
		$errmsg_arr[] = 'Tuesday close time missing. Check "Resturant is closed" if your restuarant
is closed on Tuesdays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_wednesday']))
{
$wednesday_open='0';
$wednesday_close='0';
}
else
{	
if($wednesday_open == 'time') {
		$errmsg_arr[] = 'Wednesday open time missing. Check "Resturant is closed" if your restuarant
is closed on Wednesdays';
		$errflag = true;
	}
	
	if($wednesday_close == 'time') {
		$errmsg_arr[] = 'Wednesday close time missing. Check "Resturant is closed" if your restuarant
is closed on Wednesdays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_thursday']))
{
$thursday_open='0';
$thursday_close='0';
}
else
{	
if($thursday_open == 'time') {
		$errmsg_arr[] = 'Thursday open time missing. Check "Resturant is closed" if your restuarant
is closed on Thursdays';
		$errflag = true;
	}
	
	if($thursday_close == 'time') {
		$errmsg_arr[] = 'Thursday close time missing. Check "Resturant is closed" if your restuarant
is closed on Thursdays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_friday']))
{
$friday_open='0';
$friday_close='0';
}
else
{	
if($friday_open == 'time') {
		$errmsg_arr[] = 'Friday open time missing. Check "Resturant is closed" if your restuarant
is closed on Fridays';
		$errflag = true;
	}
	
	if($friday_close == 'time') {
		$errmsg_arr[] = 'Friday close time missing. Check "Resturant is closed" if your restuarant
is closed on Fridays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_saturday']))
{
$saturday_open='0';
$saturday_close='0';
}
else
{	
if($saturday_open == 'time') {
		$errmsg_arr[] = 'Saturday open time missing. Check "Resturant is closed" if your restuarant
is closed on Saturdays';
		$errflag = true;
	}
	
	if($saturday_close == 'time') {
		$errmsg_arr[] = 'Saturday close time missing. Check "Resturant is closed" if your restuarant
is closed on Saturdays';
		$errflag = true;
	}
}

	
if (isset($_POST['closed_sunday']))
{
$sunday_open='0';
$sunday_close='0';
}
else
{	
if($sunday_open == 'time') {
		$errmsg_arr[] = 'Sunday open time missing. Check "Resturant is closed" if your restuarant
is closed on Sundays';
		$errflag = true;
	}
	
	if($sunday_close == 'time') {
		$errmsg_arr[] = 'Sunday close time missing. Check "Resturant is closed" if your restuarant
is closed on Sundays';
		$errflag = true;
	}
}

	if($email_address == '') {
		$errmsg_arr[] = 'Email missing';
		$errflag = true;
	}
else
{
if(isValidEmail($email_address) ==TRUE)
{
}
else
{
$errmsg_arr[] = 'Email address is not valid';
		$errflag = true;
}
}
	if($user_name == '') {
		$errmsg_arr[] = 'Login ID missing';
		$errflag = true;
	}
	if($password == '') {
		$errmsg_arr[] = 'Password missing';
		$errflag = true;
	}
	if($confirm_password == '') {
		$errmsg_arr[] = 'Confirm password missing';
		$errflag = true;
	}
	if( strcmp($password, $confirm_password) != 0 ) {
		$errmsg_arr[] = 'Passwords do not match';
		$errflag = true;
	}

	
//Check for duplicate restaurant login ID
	if($user_name != '') {
		$qry = "SELECT * FROM restaurants WHERE User_Name='$user_name'";
		$result = mysql_query($qry);
		if($result) {
			if(mysql_num_rows($result) > 0) {
				$errmsg_arr[] = 'Login ID already in use';
				$errflag = true;
			}
			@mysql_free_result($result);
		}
		else {
			die("Query failed");
		}
	}

	//Check for duplicate user login ID
	if($user_name != '') {
		$qry = "SELECT * FROM users WHERE User_Name='$user_name'";
		$result = mysql_query($qry);
		if($result) {
			if(mysql_num_rows($result) > 0) {
				$errmsg_arr[] = 'Login ID already in use';
				$errflag = true;
			}
			@mysql_free_result($result);
		}
		else {
			die("Query failed");
		}
	}

	
	//Check for duplicate restaurant email
	if($email_address != '') {
		$qry = "SELECT * FROM restaurants WHERE Email_Address='$email_address'";
		$result = mysql_query($qry);
		if($result) {
			if(mysql_num_rows($result) > 0) {
				$errmsg_arr[] = 'Email Address already in use';
				$errflag = true;
			}
			@mysql_free_result($result);
		}
		else {
			die("Query failed");
		}
	}


	//Check for duplicate user email
	if($email_address != '') {
		$qry = "SELECT * FROM users WHERE Email_Address='$email_address'";
		$result = mysql_query($qry);
		if($result) {
			if(mysql_num_rows($result) > 0) {
				$errmsg_arr[] = 'Email Address already in use';
				$errflag = true;
			}
			@mysql_free_result($result);
		}
		else {
			die("Query failed");
		}
	}
	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: restaurant_registration.php");
		exit();
	}

$profile_url = $user_name;
$profile_url .= ".php";
	//Create INSERT query
	$qry = "INSERT INTO restaurants(Restaurant_Name, Category, Email_Address, User_Name, Password, Profile_Url) VALUES('$restaurant_name', '$chosen_category', 
        '$email_address', '$user_name','".md5($_POST['password'])."', '$profile_url')";
	$result = @mysql_query($qry);
	
	//Check whether the query was successful or not
	if($result) {  

$old = 'restaurant.txt';

$new = "new.txt";
copy($old, $new) or die("Unable to copy $old to $new.");

$new1 = $user_name;
$new1 .= ".php";
rename($new, $new1) or die("Unable to rename $new to $new1.");

$_SESSION['SESS_USER_NAME'] = $user_name;
$_SESSION['SESS_EMAIL_ADDRESS'] = $email_address;
$_SESSION['SESS_PHONE']=$phone;
$_SESSION['SESS_WEBSITE']=$website;
$_SESSION['SESS_STREET']=$street;
$_SESSION['SESS_CITY']=$city;
$_SESSION['SESS_STATE']=$state;
$_SESSION['SESS_ZIPCODE']=$zipcode;
$country= 'United States';
$_SESSION['SESS_COUNTRY']=$country;
$_SESSION['SESS_DESCRIPTION']=$description;
$_SESSION['SESS_MONDAY_OPEN']=$monday_open;
$_SESSION['SESS_MONDAY_CLOSE']=$monday_close;
$_SESSION['SESS_TUESDAY_OPEN']=$tuesday_open;
$_SESSION['SESS_TUESDAY_CLOSE']=$tuesday_close;
$_SESSION['SESS_WEDNESDAY_OPEN']=$wednesday_open;
$_SESSION['SESS_WEDNESDAY_CLOSE']=$wednesday_close;
$_SESSION['SESS_THURSDAY_OPEN']=$thursday_open;
$_SESSION['SESS_THURSDAY_CLOSE']=$thursday_close;
$_SESSION['SESS_FRIDAY_OPEN']=$friday_open;
$_SESSION['SESS_FRIDAY_CLOSE']=$friday_close;
$_SESSION['SESS_SATURDAY_OPEN']=$saturday_open;
$_SESSION['SESS_SATURDAY_CLOSE']=$saturday_close;
$_SESSION['SESS_SUNDAY_OPEN']=$sunday_open;
$_SESSION['SESS_SUNDAY_CLOSE']=$sunday_close;
		

            
	
		                                header("location: restaurant_registration_success.php");
		                                exit();
}
	                                                   
                                   else 
                                                 {
		                                die("Query failed");
	                                         }
	          

?>