<?php
	//Start session
	session_start();


	
	//Unset the variables stored in session
	unset($_SESSION['SESS_USER_ID']);
        unset($_SESSION['SESS_RESTAURANT_ID']);
	unset($_SESSION['SESS_FIRST_NAME']);
	unset($_SESSION['SESS_LAST_NAME']);
        unset($_SESSION['SESS_EMAIL_ADDRESS']);
	unset($_SESSION['SESS_PRIV']);
	unset($_SESSION['SESS_STATUS']);
        unset($_SESSION['SESS_IMAGE']);

include('menu.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Logged Out</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
</head>
<body>
<h1 style="text-align:center">Logout </h1>
<p align="center">&nbsp;</p>
<h4 align="center" class="err">You have been logged out.</h4>
<p align="center">Click <a href="default.php">here</a> to go to Home Page</p>
</body>
</html>
