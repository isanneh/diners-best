<?php

	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

//email format
	function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

//name format

function isValidName($name){
return eregi ("^[a-zA-Z][a-zA-Z -]*$", $name);
}


	
	//Sanitize the POST values
	$title = clean($_POST['title']);
	$description = clean($_POST['description']);
	$points = clean($_POST['points']);

	
	//Input Validations
	if($title == '') {
		$errmsg_arr[] = 'Title missing';
		$errflag = true;
	}


	
	if($description == '') {
		$errmsg_arr[] = 'Description missing';
		$errflag = true;
	}

	if($points == '') {
		$errmsg_arr[] = 'Points missing';
		$errflag = true;
	}

	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: edit_prize.php");
		exit();
	}


	//Create UPDATE query
	mysql_query("UPDATE `prizes` SET `Title`='$title' WHERE `Prize_Id`='".$_SESSION['SESS_EDIT_PRIZE']."'");
        mysql_query("UPDATE `prizes` SET `Description`='$description' WHERE `Prize_Id`='".$_SESSION['SESS_EDIT_PRIZE']."'");
        mysql_query("UPDATE `prizes` SET `Points`='$points' WHERE `Prize_Id`='".$_SESSION['SESS_EDIT_PRIZE']."'");
	
	
	
		header("location: restaurant_home.php");
		exit();
	
?>