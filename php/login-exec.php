<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	

	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	
	//Sanitize the POST values
	$login = clean($_POST['login']);
	$password = clean($_POST['password']);
	
	//Input Validations
	if($login == '') {
		$errmsg_arr[] = 'Login ID or Email Address missing';
		$errflag = true;
	}
	if($password == '') {
		$errmsg_arr[] = 'Password missing';
		$errflag = true;
	}
	
	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: login-form.php");
		exit();
	}



//Create query
	$qry="SELECT * FROM users WHERE `User_Name`='$login' AND `Password`='".md5($_POST['password'])."'";
	$result=mysql_query($qry);

	//Check whether the query was successful or not
	if($result) {
		if(mysql_num_rows($result) == 1) {
			//Login Successful
			
			$login_check='1';
		                                     } //end mysql_num result
               else
                {
			//Login failed
			
                      $login_check='0';
		}
	                                            } //end of result
          else {
		die("Query failed");
	      }
	
if($login_check=='1')
{
//session_regenerate_id();
			$member = mysql_fetch_assoc($result);
			$_SESSION['SESS_USER_ID'] = $member['User_Id'];
			$_SESSION['SESS_FIRST_NAME'] = $member['First_Name'];
			$_SESSION['SESS_LAST_NAME'] = $member['Last_Name'];
			$_SESSION['SESS_USER_NAME'] = $member['User_Name'];
			$_SESSION['SESS_EMAIL_ADDRESS'] = $login;
			$_SESSION['SESS_PRIV'] = 'user';
			$_SESSION['SESS_USER'] = $member['User_Name'];
                        $_SESSION['SESS_IMAGE'] = 'idle';
			$_SESSION['views'] = 1;
header("location: user_profile.php");
			exit();
}

else 
{


//Create query
	$qry2="SELECT * FROM users WHERE `Email_Address`='$login' AND `Password`='".md5($_POST['password'])."'";
	$result2=mysql_query($qry2);

	//Check whether the query was successful or not
	if($result2) {
		if(mysql_num_rows($result2) == 1) {
			//Login Successful
			
			$login_check='1';
		                                     } //end mysql_num result2
               else
                {
			//Login failed
			
                      $login_check='0';
		}
	                                            } //end of result2
          else {
		die("Query failed");
	      }
}

//restaurant user login


if($login_check=='1')
{
//session_regenerate_id();
			$member = mysql_fetch_assoc($result2);
			$_SESSION['SESS_USER_ID'] = $member['User_Id'];
			$_SESSION['SESS_FIRST_NAME'] = $member['First_Name'];
			$_SESSION['SESS_LAST_NAME'] = $member['Last_Name'];
			$_SESSION['SESS_USER_NAME'] = $member['User_Name'];
			$_SESSION['SESS_EMAIL_ADDRESS'] = $login;
			$_SESSION['SESS_PRIV'] = 'user';
			$_SESSION['SESS_USER'] = $member['User_Name'];
                        $_SESSION['SESS_IMAGE'] = 'idle';
			$_SESSION['views'] = 1;
header("location: user_profile.php");
			exit();
}

else 
{


//Create query
	$qry3="SELECT * FROM restaurants WHERE `User_Name`='$login' AND `Password`='".md5($_POST['password'])."'";
	$result3=mysql_query($qry3);

	//Check whether the query was successful or not
	if($result3) {
		if(mysql_num_rows($result3) == 1) {
			//Login Successful
			
			$login_check='1';
		                                     } //end mysql_num result2
               else
                {
			//Login failed
			
                      $login_check='0';
		}
	                                            } //end of result2
          else {
		die("Query failed");
	      }
}

//restaurant email check

if($login_check=='1')
{
//session_regenerate_id();
			$member = mysql_fetch_assoc($result3);
			$_SESSION['SESS_RESTAURANT_ID'] = $member['Restaurant_Id'];
			$_SESSION['SESS_FIRST_NAME'] = $member['First_Name'];
			$_SESSION['SESS_LAST_NAME'] = $member['Last_Name'];
			$_SESSION['SESS_USER_NAME'] = $member['User_Name'];
			$_SESSION['SESS_EMAIL_ADDRESS'] = $login;
			$_SESSION['SESS_PRIV'] = 'restaurant';
			$_SESSION['SESS_USER'] = $member['User_Name'];
                        $_SESSION['SESS_IMAGE'] = 'idle';
			$_SESSION['views'] = 1;
header("location: restaurant_profile.php");
			exit();
}

else 
{


//Create query
	$qry4="SELECT * FROM restaurants WHERE `Email_Address`='$login' AND `Password`='".md5($_POST['password'])."'";
	$result4=mysql_query($qry4);

	//Check whether the query was successful or not
	if($result4) {
		if(mysql_num_rows($result4) == 1) {
			//Login Successful
			//session_regenerate_id();
			$member = mysql_fetch_assoc($result4);
			$_SESSION['SESS_RESTAURANT_ID'] = $member['Restaurant_Id'];
			$_SESSION['SESS_FIRST_NAME'] = $member['First_Name'];
			$_SESSION['SESS_LAST_NAME'] = $member['Last_Name'];
			$_SESSION['SESS_USER_NAME'] = $member['User_Name'];
			$_SESSION['SESS_EMAIL_ADDRESS'] = $login;
			$_SESSION['SESS_PRIV'] = 'restaurant';
			$_SESSION['SESS_USER'] = $member['User_Name'];
                        $_SESSION['SESS_IMAGE'] = 'idle';
			$_SESSION['views'] = 1;
			header("location: restaurant_profile.php");
			exit();
		                                     } //end mysql_num result2
               else
                {
			//Login failed
			
                      header("location: login-failed.php");
			exit();
		}
	                                            } //end of result2
          else {
		die("Query failed");
	      }
}
                  
                  
                   
?>